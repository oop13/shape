/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeproject;

/**
 *
 * @author Sarocha
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,6);
        System.out.println("Area of rectangle (w = " + rectangle1.getW()+ ")(l = " + rectangle1.getL()+ ") is " + rectangle1.calArea());
        rectangle1.setW(4);
        rectangle1.setL(3);
        System.out.println("Area of rectangle (w = " + rectangle1.getW()+ ")(l = " + rectangle1.getL()+ ") is " + rectangle1.calArea());
        rectangle1.setW(0);
        rectangle1.setL(0);
        System.out.println("Area of rectangle (w = " + rectangle1.getW()+ ")(l = " + rectangle1.getL()+ ") is " + rectangle1.calArea());
    }
}
