/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.shapeproject;

/**
 *
 * @author Sarocha
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3,5);
        System.out.println("Area of triangle (b = " + triangle1.getB()+ ")(h = " + triangle1.getH()+ ") is " + triangle1.calArea());
        triangle1.setB(4);;
        triangle1.setH(8);
        System.out.println("Area of triangle (b = " + triangle1.getB()+ ")(h = " + triangle1.getH()+ ") is " + triangle1.calArea());
        triangle1.setB(0);
        triangle1.setH(0);
        System.out.println("Area of triangle (b = " + triangle1.getB()+ ")(h = " + triangle1.getH()+ ") is " + triangle1.calArea());
    }
}
